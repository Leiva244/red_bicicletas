var express = require("express");
var router = express.Router();
var reservaController = require("../../controllers/api/reservaControllerAPI");

router.get("/", reservaController.reserva_list);
router.post("/create", reservaController.reserva_create);
router.post("/update", reservaController.reserva_update);
router.delete("/delete", reservaController.reserva_delete);

module.exports = router;
