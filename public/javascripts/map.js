var map = L.map('main_map', {
    center: [-34.90328,-56.18816],
    zoom: 14
});

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

// L.marker([-34.90580,-56.19130]).addTo(map)
// L.marker([-34.9063505,-56.2059795]).addTo(map)
// L.marker([-34.9202023,-56.1600649]).addTo(map)

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result) {
    console.log(result);
    result.bicicletas.forEach(function(bici) {
      L.marker(bici.ubicacion, {title: bici.id}).addTo(map)
    });
  }
})