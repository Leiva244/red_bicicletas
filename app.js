require('newrelic');
require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var usuarioRouter = require("./routes/usuario");
var tokenRouter = require("./routes/token");
var bicicletasRouter = require("./routes/bicicletas");
const authAPIRouter = require("./routes/api/auth")
var bicicletasAPIRouter = require("./routes/api/bicicletas");
var usuarioAPIRouter = require("./routes/api/usuario");
var reservaAPIRouter = require("./routes/api/reserva");
const config = require("./config/config");

const Usuario = require('./models/usuario');
const Token = require('./models/token');

let store;
if (process.env.NODE_ENV === 'development'){
  store = new session.MemoryStore;
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function(error) {
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();

app.set('secretKey', 'jwt_pwd_!!223344');

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis_!!!'
}));

var mongoose = require("mongoose");

//si estoy en ambiente de desarrollo usar
//var mongoDB = 'mongodb://localhost/red_bicicletas';
//var mongoDB = 'mongodb+srv://admin:<password>@cluster0.y6hf0.mongodb.net/<dbname>?retryWrites=true&w=majority';
//sino usar
var mongoDB = process.env.MONGO_URI;

mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error: "));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "public")));

app.get('/login', (req, res) => {
  res.render('session/login');
});

app.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, usuario, info) => {
    if (err) return next(err);
    if (!usuario) return res.render('session/login', {info});
    req.logIn(usuario, (err) => {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', (req, res) => {
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword', (req, res) => {
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', (req, res) => {
  Usuario.findOne({ email: req.body.email}, (err, usuario) => {
    if (!usuario) return res.render('session/forgotPassword', {info: {message: 'No existe el email para un usuario existente.'}});

    usuario.resetPassword( (err) => {
      if (err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', (req, res) => {
  Token.findOne({token: req.params.token }, (err, token) => {
    if (!token) return res.status(400).send({ type: 'not-verified', msg:'No existe un usuario asociado al token. Verifique que su token no haya expirado.'});

    Usuario.findById(token._userId, (err, usuario) => {
      if (!usuario) return res.status(400).send({msg: 'No existe un usuario asociado al token'});
      res.render('session/resetPassword', {errors: {}, usuario: usuario});
    });
  });
});

app.post('/resetPassword', (req, res) => {
  if (req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}},
    usuario: new Usuario({email: req.body.email})});
    return;
  }
  Usuario.findOne({ email: req.body.email}, (err, usuario) => {
    usuario.password = req.body.password;
    usuario.save( (err) => {
      if (err) {
        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});
      }else{
        res.redirect('/login');
      }
    });
  });
});

app.use("/usuario", usuarioRouter);
app.use("/token", tokenRouter);
app.use("/users", usersRouter); //no se de donde salio

app.use("/bicicletas", loggedIn, bicicletasRouter);

app.use("/api/auth", authAPIRouter);
app.use("/api/bicicletas", validarUsuario, bicicletasAPIRouter);
app.use("/api/usuario", usuarioAPIRouter);
app.use("/api/reserva", reservaAPIRouter);

app.use('/privacy_policy', function(req, res) {
  res.sendFile('public/privacy_policy.html');
});

app.use('/google6db529383c8a917c', function(req, res) {
  res.sendFile('public/google6db529383c8a917c.html');
});

// app.get('/auth/google',
//   passport.authenticate('google', { scope:
//     ['https://www.googleapis.com/auth/plus.login',
//      'https://www.googleapis.com/auth/plus.profile.emails.read'] 
//   })
// );

app.get('/auth/google',
  passport.authenticate('google', { scope:
      [ 'email', 'profile' ] }
));

app.get( '/auth/google/callback',
    passport.authenticate( 'google', {
        successRedirect: '/',
        failureRedirect: '/error'
}));

app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  }else {
    console.log('Usuario sin loguearse');
    res.redirect('/login');
  }
};

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({status: "error", message: err.message, data: null});
    }else {
      
      req.body.userId = decoded.id;

      console.log('jwt verify: ' + decoded);

      next();
    }
  });
};


module.exports = app;
