var mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
var Reserva = require("./reserva");
var Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const saltRounds = 10;

const Token = require("../models/token");
const mailer = require("../mailer/mailer");

const validateEmail = function (email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    required: [true, "El email es obligatorio"],
    lowercase: true,
    unique: true,
    validate: [validateEmail, "Por favor, ingrese un email valido"],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
  },
  password: {
    type: String,
    required: [true, "La contaseña es obligatoria"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
  googleId: String,
  facebookId: String
});

usuarioSchema.plugin(uniqueValidator, {
  message: "El {PATH} ya existe con otro usuario",
});

usuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (
  usuario,
  bicicleta,
  desde,
  hasta,
  cb
) {
  var reserva = new Reserva({
    usuario: usuario ? usuario : this._id,
    bicicleta,
    desde,
    hasta,
  });
  reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return console.log(err.message);
    }

    const mailOptions = {
      from: process.env.user,
      to: email_destination,
      subject: "Verificacion de cuenta",
      html: `Hola,<br/><br/> Por favor, para verificar su cuenta haga click en este link:<br/> <a href="${process.env.HOST}/token/confirmation/${token.token}">${process.env.HOST}/token/confirmation/${token.token}</a> <br/>`,
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return console.log(err.message);
      }

      console.log(
        `Se ha enviado un email de bienvenida a: ${email_destination}.`
      );
    });
  });
};

usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return cb(err);
    }

    const mailOptions = {
      from: process.env.user,
      to: email_destination,
      subject: "Reseteo de password de cuenta",
      html: `Hola,<br/><br/> Por favor, para resetear el password de su cuenta haga click en este link:<br/> <a href="${process.env.HOST}/resetPassword/${token.token}">${process.env.HOST}/resetPassword/${token.token}</a> <br/>`,
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return console.log(err.message);
      }

      console.log(
        `Se ha enviado un email para resetear password a: ${email_destination}.`
      );
    });

    cb(null);
  });
};

const randomPass = (length) => {
  const str = "!@#$%ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let pass = "";
  for (let i = 0; i < length; i++) {
    pass += str.charAt(Math.floor(Math.random() * str.length));
  }
  return pass;
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [{ 'googleId': condition.id }, { 'email': condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("---------- CONDITION ----------");
        console.log(condition);
        let values = {};
        values.googleId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = randomPass(20);
        console.log("---------- VALUES ----------");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) {
            console.log(err);
          }
          return callback(err, result);
        });
      }
    }
  );
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [{ 'facebookId': condition.id }, { 'email': condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("---------- CONDITION ----------");
        console.log(condition);
        let values = {};
        values.facebookId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = randomPass(20);
        console.log("---------- VALUES ----------");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) {
            console.log(err);
          }
          return callback(err, result);
        });
      }
    }
  );
};

module.exports = mongoose.model("Usuario", usuarioSchema);
